<!-- Strings -->
![Strings](http://cse02-iiith.vlabs.ac.in/imgs/exp7.png)

># Definition
### A string is a __*sequence of characters*__ -- it can contain a word, your name, a sentence, a paragraph or even an entire composition. A question that arises is that how the contents of a string can be stored. An array of characters seems like a very good choice.
These functions will allow you to treat strings like variables and allow operations like addition(concatenation) of two strings, searching a string in another string, compare two strings etc.

---

># Theory
### In C programming, a string is essentially an array of ASCII code characters. Each character is stored by writing the corresponding 8-bit ASCII code of the character. So, One can initialize a string just like an array:

**char str[50]={'C',' ','P','r','o','g','r','a','m'};**<br>
**char str[50]="C programming";**

----


># Objectives
* To understand the concept of strings and how they are a special type of character arrays.
* To understand the usage of string libraries to do common string operations.
  
  ---

># Procedure
   
   ![String matching](http://cse02-iiith.vlabs.ac.in/exp7/simulation/StringMatching/index.html)
   * String  matching<br>
       1. Press start to start the experiment and select two string str1 and str2 to compare.
       2. Press next to see the execution of the code
       3. Relavant line in the code is shown here
       4. The output of the code is shown in the right
   

   ![String Comparison](http://cse02-iiith.vlabs.ac.in/exp7/images/string_comparision.png)
* String comparison<br>
      1.  Press start to start the experiment and select two string str1 and str2 to compare.
      2.  Press next to see the execution of the code
      3.  The output of the code is shown in the right
      4.  You can stop the code using stop button

---


># Extra information

1. [Wikipedia](http://en.wikipedia.org/wiki/String_(computer_science))<br>
1. [Exforsys](http://www.exforsys.com/tutorials/c-language/handling-of-character-strings-in-c.html)
   

---



 

